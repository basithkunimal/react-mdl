import {
	Button,
	Card,
	CardText,
	Layout,
	Header,
	HeaderRow,
	Drawer,
	Content,
	Navigation,
	Grid,
	Spinner
} from 'react-mdl';
import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, Link, IndexLink, browserHistory} from 'react-router';

class Home extends React.Component {
	constructor() {
		super();
		this.state = {
			spinnerShown: false
		}
		this.toggleSpinner = this.toggleSpinner.bind(this);
	}
	toggleSpinner() {
		this.setState({spinnerShown:!this.state.spinnerShown});
	}
	render() {
		var { spinnerShown } = this.state;
		var spinnerStyles = {
			width:100,
			height:50,
			display:'flex',
			flexDirection:'row',
			alignItems:'center',
			paddingLeft:10,
			position:'fixed',
			bottom:spinnerShown ? 20 : -60,
			right:20,
			transition:'.3s bottom',
			color: '#999'
		}
		return (
			<Grid>
				<Button onClick={this.toggleSpinner}>Helloo</Button>
				<div className="mdl-shadow--2dp" style={spinnerStyles}>
					<Spinner style={{marginRight:10}} /> Loading
				</div>
			</Grid>
		);
	}
}

class Root extends React.Component {
	render() {
		return (
			<Layout fixedDrawer>
				<Drawer>
					<Navigation>
						<IndexLink to="/">Home</IndexLink>
					</Navigation>
				</Drawer>
				<Header>
					<HeaderRow title="Hello World">

					</HeaderRow>
				</Header>
				<Content>
					{this.props.children}
				</Content>
			</Layout>
		);
	}
}

class App extends React.Component {

	render() {
		return (
			<Router history={browserHistory}>
				<Route path="/" component={Root}>
					<IndexRoute component={Home} />
				</Route>
			</Router>
		);
	}

}

ReactDOM.render(
	<App />,
	document.getElementById('container')
);
