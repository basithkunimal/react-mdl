var fs = require("fs");
var browserify = require("browserify");
browserify("./app.js")
  .transform("babelify", {presets: ["es2015", "react"]})
  .bundle()
  .pipe(fs.createWriteStream("bundle.js"));

// var fs = require('fs');
// var browserSync = require('browser-sync');
// var browserify = require('browserify');
// var watchify = require('watchify');

// var b = browserify({
//   entries: ['./app.js'],
//   cache: {},
//   packageCache: {},
//   plugin: [watchify]
// });

// b.on('update', bundle);
// bundle();

// function bundle() {
//   b.transform("babelify", {presets: ["es2015", "react"]})
//   .bundle()
//   .pipe(fs.createWriteStream('bundle.js'));
// }